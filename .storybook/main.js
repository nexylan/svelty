const preprocess = require('../preprocess');

module.exports = {
  stories: [
    // Ensures this file will always be the first page.
    '../src/docs/Index.stories.mdx',
    '../src/docs/**/*.stories.mdx',
    '../src/components/**/*.stories.*',
  ],
  addons: [
    '@storybook/addon-essentials',
    '@storybook/addon-a11y',
  ],
  webpackFinal: async (config) => {
    const svelteLoader = config.module.rules.find(
      (r) => r.loader && r.loader.includes('svelte-loader'),
    );
    svelteLoader.options.preprocess = preprocess;
    return config;
  },
};
