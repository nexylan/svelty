const { colors } = require('tailwindcss/defaultTheme');

const backgrounds = Object.entries(colors).map(([name, value]) => ({
  name,
  value: typeof value === 'object' ? value[500] : value,
}));

export const parameters = {
  backgrounds: {
    default: backgrounds[0].name,
    values: backgrounds,
  },
};

export default null;
