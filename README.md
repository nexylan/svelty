# Svelty

A set of high quality components. Built with Svelte, designed by TailwindCSS.

## Documentation

https://svelty.nexylan.dev/

## Usage

See our [documentation website](https://svelty.nexylan.dev/).

## Development

To edit and contribute to Svelty, simply run the following command:

```
make
```

A Storybook instance will appears to your favorite browser. Start coding! 🚀
