import Container from '../views/Container.svelte';
import DataTable from './DataTable.svelte';
import { users } from '../props';

export default {
  title: 'Components/Data/DataTable',
  component: Container,
};

export const Default = (args) => ({
  Component: Container,
  props: {
    component: DataTable,
    props: {
      ...args,
      columns: users.columns,
      rows: users.rows,
    },
  },
});
Default.args = {
  showHeader: true,
  showFooter: true,
  hoverClass: 'hover:bg-gray-200',
};

export const noHeaders = () => ({
  Component: Container,
  props: {
    component: DataTable,
    props: {
      columns: users.columns,
      rows: users.rows,
      showHeader: false,
      showFooter: false,
    },
  },
});

export const Empty = (args) => ({
  Component: Container,
  props: {
    component: DataTable,
    props: {
      ...args,
      columns: users.columns,
    },
  },
});
Empty.args = {
  emptyDataMessage: 'No data.',
  showHeader: true,
  showFooter: true,
};
