import Container from '../views/Container.svelte';
import DataView from './DataView.svelte';
import DataViewLocalView from '../views/DataViewLocalView.svelte';
import DataViewRemoteView from '../views/DataViewRemoteView.svelte';
import { users, dataSourceDefinitions } from '../props';

const sourceOptions = Object.keys(dataSourceDefinitions);

export default {
  title: 'Components/Data/DataView',
  component: Container,
  argTypes: {
    source: {
      control: {
        type: 'select',
        options: sourceOptions,
      },
    },
  },
};

export const Default = (args) => ({
  Component: Container,
  props: {
    component: DataView,
    props: {
      ...args,
      ...users,
    },
  },
});
Default.args = {
  filter: true,
};

export const LocalData = () => ({
  Component: Container,
  props: {
    component: DataViewLocalView,
  },
});

export const RemoteData = (args) => ({
  Component: Container,
  props: {
    component: DataViewRemoteView,
    props: args,
  },
});
RemoteData.args = {
  source: sourceOptions[0],
};
