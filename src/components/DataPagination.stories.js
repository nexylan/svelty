import Container from '../views/Container.svelte';
import DataPagination from './DataPagination.svelte';

export default {
  title: 'Components/Data/DataPagination',
  component: Container,
};

export const Default = (args) => ({
  Component: Container,
  props: {
    component: DataPagination,
    props: args,
  },
});
Default.args = {
  current: 5,
  last: 8,
};

export const Begin = () => ({
  Component: Container,
  props: {
    component: DataPagination,
    props: {
      current: 1,
      last: 8,
    },
  },
});

export const End = () => ({
  Component: Container,
  props: {
    component: DataPagination,
    props: {
      current: 8,
      last: 8,
    },
  },
});

export const Large = () => ({
  Component: Container,
  props: {
    component: DataPagination,
    props: {
      current: 9,
      last: 50,
    },
  },
});

export const ThreePages = () => ({
  Component: Container,
  props: {
    component: DataPagination,
    props: {
      current: 2,
      last: 3,
    },
  },
});

export const TwoPages = () => ({
  Component: Container,
  props: {
    component: DataPagination,
    props: {
      current: 1,
      last: 2,
    },
  },
});

export const OnePage = () => ({
  Component: Container,
  props: {
    component: DataPagination,
    props: {
      current: 1,
      last: 1,
    },
  },
});
