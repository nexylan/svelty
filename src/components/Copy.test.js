import { render } from '@testing-library/svelte';
import Copy from './Copy.svelte';

test('Copy ', async () => {
  const { container } = await render(Copy, {
    props: {
      content: 'test-copy',
    },
  });
  expect(container.innerHTML.includes('test-copy')).toBe(true);
});
