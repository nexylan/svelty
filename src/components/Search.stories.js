import Container from '../views/Container.svelte';
import Search from './Search.svelte';
import { users } from '../props';

export default {
  title: 'Components/Search',
  component: Container,
};

export const Default = (args) => ({
  Component: Container,
  props: {
    component: Search,
    props: {
      ...args,
      fields: users.searchFields,
    },
  },
});
Default.args = {
  rounded: true,
};

export const NoProp = () => ({
  Component: Container,
  props: {
    component: Search,
  },
});

export const WithContent = () => ({
  Component: Container,
  props: {
    component: Search,
    props: {
      search: 'Test',
    },
  },
});
