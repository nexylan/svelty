import Container from '../views/Container.svelte';
import DataFilter from './DataFilter.svelte';
import { users } from '../props';

export default {
  title: 'Components/Data/DataFilter',
  component: Container,
};

export const Default = (args) => ({
  Component: Container,
  props: {
    component: DataFilter,
    props: {
      ...args,
      sortFields: users.sortFields,
      searchFields: users.searchFields,
      sortBy: users.sortBy,
      sortAsc: users.sortAsc,
    },
  },
});
Default.args = {
  rounded: true,
};

export const NoSort = () => ({
  Component: Container,
  props: {
    component: DataFilter,
  },
});
NoSort.args = {
  rounded: true,
};
