import Container from '../views/Container.svelte';
import Copy from './Copy.svelte';
import CopyWithCustomTextView from '../views/CopyWithCustomTextView.svelte';

export default {
  title: 'Components/Copy',
  component: Container,
};

export const Default = (args) => ({
  Component: Container,
  props: {
    component: Copy,
    props: args,
  },
});
Default.args = {
  content: 'Copy me!',
  showContent: true,
  title: 'Copy content',
  timerDuration: 1000,
  iconCopy: 'far fa-copy',
  iconSuccess: 'far fa-check-circle',
  iconLarge: false,
};

export const WithCustomText = () => ({
  Component: Container,
  props: {
    component: CopyWithCustomTextView,
  },
});

export const Emoji = () => ({
  Component: Container,
  props: {
    component: Copy,
    props: {
      content: '😀 😎 👍 💯 🙃',
    },
  },
});
