import Container from '../views/Container.svelte';
import SortBy from './SortBy.svelte';
import { users } from '../props';

export default {
  title: 'Components/SortBy',
  component: Container,
};

export const Default = (args) => ({
  Component: Container,
  props: {
    component: SortBy,
    props: {
      ...args,
      fields: users.sortFields,
    },
  },
});
Default.args = {
  rounded: true,
};
