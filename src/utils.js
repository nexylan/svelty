export const polyfillValue = (value) => {
  const requiredFields = [
    ['value', [
      'name',
      'title',
      'id',
    ]],
    ['label', [
      'id',
      'name',
      'title',
    ]],
  ];
  let polyfilled = {};

  requiredFields.forEach(([requiredField, alternatives]) => {
    // Scalar values are just transformed onto object representation.
    if (typeof value !== 'object') {
      polyfilled[requiredField] = value;
      return;
    }

    // Starts with the value object, then fill the missing required fields.
    polyfilled = value;
    if (typeof value[requiredField] === 'undefined') {
      alternatives.forEach((alternative) => {
        if (typeof value[alternative] !== 'undefined') {
          polyfilled[requiredField] = value[alternative];
        }
      });
    }
  });

  return polyfilled;
};

export const polyfillRow = (row) => {
  const polyfilled = {};

  Object.entries(row).forEach(([key, value]) => {
    polyfilled[key] = polyfillValue(value);
  });

  return polyfilled;
};

export default null;
