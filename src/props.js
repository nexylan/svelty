/* eslint-disable no-underscore-dangle */
import faker from 'faker';
import queryString from 'query-string';
import parseLinkHeader from 'parse-link-header';

const sd = 42;

faker.seed(sd);
export const countries = {
  columns: [
    ['id', 'ID'],
    ['name', 'Name'],
    ['deliverable', 'Deliverable'],
  ],
  rows: Array.from({ length: 4 }, (_, i) => ({
    id: i + 1,
    name: faker.address.country(),
    deliverable: faker.random.boolean(),
  })),
};

faker.seed(sd);
export const users = {
  sortFields: {
    id: 'ID',
    name: 'Name',
    email: 'Email address',
  },
  sortBy: 'id',
  sortAsc: true,
  searchFields: [
    ['enabled', 'Enabled', [true, false]],
    ['country', 'Country', countries.rows],
  ],
  columns: [
    ['id', 'ID'],
    ['name', 'Name'],
    ['email', 'Email address'],
    ['enabled', 'Enabled'],
    ['country', 'Country'],
  ],
  rows: Array.from({ length: 15 }, (_, i) => {
    const countryId = faker.random.number(countries.rows.length - 1);
    const enabled = faker.random.boolean();

    return {
      id: {
        value: i + 1,
        label: `#${i + 1}`,
        link: `https://httpbin.org/${i + 1}/${i + 1}`,
      },
      name: faker.name.findName(),
      ...i === 3 ? {} : { email: faker.internet.email() },
      enabled: {
        value: enabled,
        label: enabled ? 'Enabled' : 'Disabled',
      },
      country: {
        ...countries.rows[countryId],
      },
    };
  }),
};

export const dataSourceDefinitions = {
  'jsonplaceholder.typicode.com': {
    filter: true,
    sortFields: {
      id: 'ID',
      name: 'Name',
      email: 'Email address',
    },
    sortBy: 'id',
    searchFields: () => [
      [
        'postId',
        'Post',
        () => fetch('https://jsonplaceholder.typicode.com/posts').then((response) => {
          if (!response.ok) {
            throw Error('Error fetching data.');
          }

          return response.json();
        }),
      ],
      [
        'fake',
        'Not Working',
        () => fetch('https://jsonplaceholder.typicode.com/tototatatititutu').then((response) => {
          if (!response.ok) {
            throw Error('Error fetching data.');
          }

          return response.json();
        }),
      ],
    ],
    columns: [
      ['id', 'ID'],
      ['post', 'Post'],
      ['name', 'Name'],
      ['email', 'Email address'],
    ],
    fetch: async (page = 1, sortBy = 'id', sortAsc = true, search = '', query = {}) => {
      const posts = await fetch('https://jsonplaceholder.typicode.com/posts').then((response) => response.json());
      const url = 'https://jsonplaceholder.typicode.com/comments';
      const parameters = {
        _page: page,
        _limit: 10,
        _sort: sortBy,
        _order: sortAsc ? 'asc' : 'desc',
        q: search,
      };

      Object.keys(query).forEach((field) => {
        Object.keys(query[field]).forEach((condition) => {
          const builtField = `${field}${condition === '_eq' ? '' : condition}`;
          parameters[builtField] = query[field][condition];
        });
      });

      const fetchResource = `${url}?${queryString.stringify(parameters)}`;
      const links = (await fetch(fetchResource, { method: 'HEAD' })).headers.get('Link');

      return {
        lastPage: links ? parseInt(parseLinkHeader(links).last._page, 10) : 1,
        rows: fetch(fetchResource)
          .then((response) => response.json())
          .then((comments) => comments.map((comment) => ({
            ...comment,
            post: posts.filter((post) => post.id === comment.postId)[0],
          }))),
      };
    },
  },
  'reqres.in': {
    filter: false,
    sortFields: {},
    searchFields: () => [],
    sortBy: 'id',
    columns: [
      ['id', 'ID'],
      ['email', 'Email address'],
      ['first_name', 'First name'],
      ['last_name', 'First name'],
    ],
    fetch: async (page = 1) => {
      const url = 'https://reqres.in/api/users';
      const parameters = {
        page,
        per_page: 5,
        delay: 2,
      };

      return fetch(`${url}?${queryString.stringify(parameters)}`)
        .then((response) => response.json())
        .then((json) => ({
          lastPage: json.total_pages,
          rows: json.data,
        }));
    },
  },
};

export default null;
