const tailwindcss = require('tailwindcss');
const sveltePreprocess = require('svelte-preprocess');

module.exports = sveltePreprocess({
  postcss: {
    plugins: [
      tailwindcss,
    ],
  },
});
