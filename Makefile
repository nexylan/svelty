all: install test up

install:
	npm install

test:
	npm run test-watch

up:
	npm run storybook
